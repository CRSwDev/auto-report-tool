# README #

This readme is for the Auto Reporting and Subsampling program.

# Summary #

* Version: 1.1

* The purpose of this program is to automate the subsampling process after the sequencing runs are completed for
designated runs. After the program is triggered it will calculate the number of reads to subsample and then recreate a 
new yml file with the read subsample number.

* The program can be run by the command 
```commandline
python AutoReport.py --listdirect /PATHS/TO/DIRECTORIES/WITH/METRICS_SUMMARY.CSV
```

* Example command line:
```commandline
python AutoReport.py --listdirect /Users/10279131/Desktop/tempMetrics/autoproj/MIST-results/20200129_20200122MPKcsV1RhapTraining1_20200122MPKcsCtrl1V1RhapTraining1 /Users/10279131/Desktop/tempMetrics/autoproj/MIST-results/20200129_20200122MPKcsV1RhapTraining1_20200122MPKcsTest1V1RhapTraining1 /Users/10279131/Desktop/tempMetrics/autoproj/MIST-results/20200129_20200122MPKcsV1RhapTraining1_20200122MPKcsTest2V1RhapTraining1
```

# How do I get set up? #

* Dependencies
    - Pandas
    - Numpy
    
* How to run tests

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* [Imteaz Siddique](imteaz.siddique@bd.com)
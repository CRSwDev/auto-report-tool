import pandas as pd
from pathlib2 import PurePath
import numpy as np

'''
The purpuse of this script is to open a pre-existing yml file and then add on
the subsample calculation at the end of the yml file.
'''


def ymlgen(dfList, pathList):
    ymlPath = PurePath('/STORAGE/Resolve/analysis-config')
    ymlNames = []
    df_index = list(dfList[0].index)

    mrna = dfList[0]
    abc = dfList[1]
    tcr = dfList[2]
    bcr = dfList[3]

    for paths in pathList:
        tempPath = PurePath(paths)
        tempTuple = tempPath.parts
        tempName = tempTuple[-1][9:]
        ymlNames.append(tempName)

    for ymlFiles, rowID in zip(ymlNames, df_index):
        mrnaRow = mrna.loc[rowID]
        abcRow = abc.loc[rowID]
        tcrRow = tcr.loc[rowID]
        bcrRow = bcr.loc[rowID]
        fullPath = ymlPath.joinpath(ymlFiles + '.yml')
        with open(str(fullPath), 'r') as fh:
            currYML = fh.read()
        fh.close()

        newName = ymlFiles + '-subsamp-' + str(mrnaRow['Raw Downsample']) + '.yml'  # need to change naming structure

        subsample = '\n' + '# subsample,' + mrnaRow['Library'] + ',' + str(mrnaRow['Raw Downsample']) + '\n'
        if abcRow['Library'] is not np.nan:
            subsample = subsample + '# subsample,' + abcRow['Library'] + ',' + str(abcRow['Raw Downsample']) + '\n'
        if tcrRow['Library'] is not np.nan:
            subsample = subsample + '# subsample,' + tcrRow['Library'] + ',' + str(tcrRow['Raw Downsample']) + '\n'
        if bcrRow['Library'] is not np.nan:
            subsample = subsample + '# subsample,' + bcrRow['Library'] + ',' + str(bcrRow['Raw Downsample']) + '\n'
        else:
            continue

        outPath = ymlPath.joinpath(newName)
        newYML = currYML + subsample
        with open(str(outPath), 'w') as wh:
            wh.write(newYML)
        wh.close()



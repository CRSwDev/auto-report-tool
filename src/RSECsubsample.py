# -*- coding: utf-8 -*-
"""
RSECSubsample.py
Created on Wed Nov 27 07:21:47 2019

@author: Tracy Campbell

"""


import pandas as pd
import numpy as np
from datetime import datetime, date, time
import argparse




parser = argparse.ArgumentParser(description='Calculates subsampling levels for MIST Results')
parser.add_argument('--path', nargs =1, type=str, help='Path to Aggregated MIST Summary')
parser.add_argument('--filename', type=str, help='Name of Aggregated MIST summary. Must be csv format.')
parser.add_argument('--RSEC', type=bool, default=False, help = "Type 'True' to subsample to RSEC. False to subsample to Raw reads. Default is False." )
args = parser.parse_args()

print(args.filename[0])
path = args.path[0]
print(type(args.path))
filename  = args.filename
RSEC = args.RSEC


##==============================================================================
##For Troubleshooting
#path = 'C:\\Users\\10275231\\BD\\V2 Beads - Experiments\\V2B59-20190927-TC-DoublePE-JR\\V2B59_JR_MIST_Results\\SubsampleIV'
#filename = '20191119_MIST_subsampleIVb.csv'
#RSEC = 'True'
#MIST = pd.read_csv(path + '\\' + filename, header=0, index_col=0)
##==========================================================

#Input file assumes columns are sample names
#Rows (fields) must include:
#Run Name, Total Reads, # of cells called, Mean RSEC reads per cell
#Raw Reads per cell (calculated from output, must be total reads/putative cells)
#See Sample file for more detail

MIST = pd.read_csv(path + '\\'  + filename, header=0, index_col=0)
MIST = MIST.transpose()

#convert columns to float
floatcols = ['Total reads', '# of cells called', 'Mean RSEC reads per cell']
MIST[floatcols] = MIST[floatcols].astype(float)

MIST['Raw Reads per Cell'] = MIST['Total reads'] / MIST['# of cells called']

RawMIN = MIST['Raw Reads per Cell'].min()
RSECMIN = MIST['Mean RSEC reads per cell'].min() 

MIST['RSEC Percent Over'] = ((MIST['Mean RSEC reads per cell'] - RSECMIN)/MIST['Mean RSEC reads per cell']) 
MIST['RSEC Downsample'] = (1 - MIST['RSEC Percent Over'])* MIST['Total reads']

MIST['Raw Percent Over'] = ((MIST['Raw Reads per Cell'] - RawMIN)/MIST['Raw Reads per Cell']) 
MIST['Raw Downsample'] = (1 - MIST['Raw Percent Over'])* MIST['Total reads']
MIST['RSEC Resample?'] = np.absolute(MIST['RSEC Percent Over']) > .049 #could change this to user input
MIST['Raw Resample?'] = np.absolute(MIST['Raw Percent Over']) > .049

#Current version just writes csv file that subsamples. 
#Future version would test to see if min/max are within 5% of eachother
writecolumns = ['SampleID','Run Name','Total reads','Mean RSEC reads per cell','RSEC Percent Over',\
                'RSEC Downsample','RSEC Resample?','Raw Reads per Cell',\
                'Raw Percent Over', 'Raw Downsample','Raw Resample?']
dt=datetime.now()  
t = dt.strftime('%d%b%Y_%H%M_%S')
MIST[writecolumns].to_csv(path + '\\' + filename[:-4] + '_CalcSubsample_' + t +'.csv')
   

# -*- coding: utf-8 -*-
"""
RSECSubsample.py
Created on Wed Nov 27 07:21:47 2019

@author: Tracy Campbell
addendum: This code pulls in the dataframe and helps subsample the reads

"""

import pandas as pd

##==============================================================================
##For Troubleshooting
# path = 'C:\\Users\\10275231\\BD\\V2 Beads - Experiments\\V2B59-20190927-TC-DoublePE-JR\\V2B59_JR_MIST_Results\\SubsampleIV'
# filename = '20191119_MIST_subsampleIVb.csv'
# RSEC = 'True'
# MIST = pd.read_csv(path + '\\' + filename, header=0, index_col=0)
##==========================================================

# Input file assumes columns are sample names
# Rows (fields) must include:
# Run Name, Total Reads, # of cells called, Mean RSEC reads per cell
# Raw Reads per cell (calculated from output, must be total reads/putative cells)
# See Sample file for more detail

def SubSampCalc(rhapdf):
    MIST = rhapdf.drop(columns='Library').astype('float')

    MIST['Raw Reads per Cell'] = MIST['Total_Reads_in_FASTQ'] / MIST['Putative_Cell_Count']

    RawMIN = MIST['Raw Reads per Cell'].min()
    RSECMIN = MIST['Mean_Reads_per_Cell'].min()

    MIST['RSEC Percent Over'] = ((MIST['Mean_Reads_per_Cell'] - RSECMIN) / MIST['Mean_Reads_per_Cell'])
    MIST['RSEC Downsample'] = (1 - MIST['RSEC Percent Over']) * MIST['Total_Reads_in_FASTQ']

    MIST['Raw Percent Over'] = ((MIST['Raw Reads per Cell'] - RawMIN) / MIST['Raw Reads per Cell'])
    MIST['Raw Downsample'] = (1 - MIST['Raw Percent Over']) * MIST['Total_Reads_in_FASTQ']

    rhapdf = rhapdf.join(MIST[['RSEC Downsample', 'RSEC Percent Over', 'Raw Downsample', 'Raw Percent Over']])
    # Current version just writes csv file that subsamples.
    # Future version would test to see if min/max are within 5% of eachother
    return rhapdf

def SubSample(dflist):
    newdfList = []

    for items in dflist:
        newdf = SubSampCalc(items)
        newdfList.append(newdf)

    return newdfList


import pandas as pd
import matplotlib.pyplot as plt

def RhapDataVis(rhapdf):
    plt.style.use("ggplot")
    figpath = '/Users/10279131/Desktop/tempMetrics/autoproj/figures'
    data = rhapdf
    x_values = list(data.index.values)
    recorrect = []
    for items in x_values:
        nameFix = items.rstrip("_Metrics_Summary.csv")
        recorrect.append(nameFix)

    plt.bar(x=recorrect, height="Mean_Molecules_per_Cell", data=data)
    plt.title("Mean RSEC mols/cell")
    plt.ylabel("mols per cell")
    plt.savefig(figpath + "/bpmolspercell.png")
    plt.clf()

    plt.bar(x=recorrect, height="Mean_RSEC_Sequencing_Depth", data=data)
    plt.title("Mean RSEC Depth")
    plt.ylabel("RSEC Depth")
    plt.savefig(figpath + "/bpseqdepth.png")
    plt.clf()

    plt.bar(x=recorrect, height="Mean_Reads_per_Cell", data=data)
    plt.title("Mean RSEC reads/cell")
    plt.ylabel("RSEC RSEC reads/cell")
    plt.savefig(figpath + "/readspercell.png")
    plt.clf()
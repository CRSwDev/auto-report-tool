# Author: Imteaz Siddique
# Scope: The purpose of this script is to parse through multiple mRNA library metrics summary and generate a table

from pathlib2 import Path
import pandas as pd
from StringIO import StringIO


def RhapTable(filePath):
    dfColumns = ['Total_Reads_in_FASTQ', 'Mean_Reads_per_Cell', 'Putative_Cell_Count', 'Mean_Molecules_per_Cell',
                 'Mean_Targets_per_Cell', 'Library']

    mrna_workdf = pd.DataFrame(columns=dfColumns)
    abseq_workdf = pd.DataFrame(columns=dfColumns)
    tcr_workdf = pd.DataFrame(columns=dfColumns)
    bcr_workdf = pd.DataFrame(columns=dfColumns)

    for files in filePath:
        path = list(Path(files).glob('*Metrics_Summary.csv'))
        name = path[0].name.rstrip('_Metrics_Summary.csv')
        mrna_workdf = mrna_workdf.append(pd.Series(name=name))
        abseq_workdf = abseq_workdf.append(pd.Series(name=name))
        tcr_workdf = tcr_workdf.append(pd.Series(name=name))
        bcr_workdf = bcr_workdf.append(pd.Series(name=name))
        with open(str(path[0]), 'r') as fh:
            data = fh.read()
        fh.close()
        dataiter = data.split('\r\n').__iter__()

        seq_qual = str()
        cells_rsec = str()

        for row in dataiter:
            # look for lines that have a ''
            if row == '#Sequencing Quality#':
                row = dataiter.next()
                while row != '':
                    seq_qual += row + '\n'
                    row = dataiter.next()
            if row == '#Cells RSEC#':
                row = dataiter.next()
                while row != '':
                    cells_rsec += row + '\n'
                    row = dataiter.next()

        seq_qual_df = pd.read_csv(StringIO(seq_qual))
        cells_rsec_df = pd.read_csv(StringIO(cells_rsec))

        for index, series in seq_qual_df.iterrows():
            if ('rhap' or 'mrna') in series['Library'].lower():
                for seriesindex, values in series.iteritems():
                    if seriesindex in dfColumns:
                        mrna_workdf[seriesindex][name] = values
            if ('abc' or 'abseq') in series['Library'].lower():
                for seriesindex, values in series.iteritems():
                    if seriesindex in dfColumns:
                        abseq_workdf[seriesindex][name] = values
            if 'bcr' in series['Library'].lower():
                for seriesindex, values in series.iteritems():
                    if seriesindex in dfColumns:
                        bcr_workdf[seriesindex][name] = values
            if 'tcr' in series['Library'].lower():
                for seriesindex, values in series.iteritems():
                    if seriesindex in dfColumns:
                        tcr_workdf[seriesindex][name] = values

        for index, series in cells_rsec_df.iterrows():
            if 'mrna' in series['Target_Type'].lower():
                for seriesindex, values in series.iteritems():
                    if seriesindex in dfColumns:
                        mrna_workdf[seriesindex][name] = values
            if series['Target_Type'].lower() == 'abseq':
                for seriesindex, values in series.iteritems():
                    if seriesindex in dfColumns:
                        abseq_workdf[seriesindex][name] = values

        tcr_workdf['Putative_Cell_Count'][name] = mrna_workdf['Putative_Cell_Count'][name]
        bcr_workdf['Putative_Cell_Count'][name] = mrna_workdf['Putative_Cell_Count'][name]

    dflist = [mrna_workdf, abseq_workdf, tcr_workdf, bcr_workdf]
    return dflist

import argparse
import os
import sys
import time
import glob


def main():
    parser = argparse.ArgumentParser(add_help=True)
    parser.add_argument('--path', action='store', dest='path', required=True,
                        help="The full fastq path to sample directory, /project/sample/."
                             "If backup, most likely starts with /NAS01")
    parser.add_argument('--gene-panel', action='store', dest='ref', required=True,
                        help="Required, file name of gene panel on server under gene-panel. Do not include .fasta at end.")
    parser.add_argument('--ab-panel', action='store', dest='abseq', required=True,
                        help="Optional, file name if second abseq panel is used in analysis. Under abseq-panel on server.")
    parser.add_argument('--label-version', action='store', type=int, dest='label_version', default=2,
                        choices=[1, 2], help="Specify which version of the cell label you are using. "
                                             "Specify '2' (default) for 9mer, '1' for 8mer.")
    parser.add_argument('--trueno', action='store', dest='trueno',
                        help="For trueno run, argument is hs for human sample tag, and mm for mouse.")
    parser.add_argument('--subsample', action='store', dest='subsample',
                        help="Number to subsample OR value 0<x<1 for percentage of total."
                             "Make sure to include the decimal, so 100 would be 100.0")
    parser.add_argument('--subsample-seed', action='store', dest='seed', help="Subsample seed.")
    parser.add_argument('--bam-input', action='store', dest='bam',
                        help="Option to add Bam output from previous runs to this run."
                             "Comma separate, no space.")
    parser.add_argument('--slack', action='store', dest='slack', help="Enter name to get slack messages, optional.")
    args = parser.parse_args()

    list_path = args.path.split('/')
    if len(list_path) < 2:
        print
        "Error: Fastq path needs parent directory. If fastq in current directory use './sample.fastq.gz'."
    project = list_path[-2]
    project = list_path[-2]

    R1 = combine_fastqs(args.path, 'R1', project, list_path[-1], args.trueno)
    R2 = combine_fastqs(args.path, 'R2', project, list_path[-1], args.trueno)

    fastqs = [R1, R2]

    yml = '/STORAGE/Resolve/analysis-config/' + project + '_' + list_path[-1] + '.yml'
    make_yml(fastqs, yml, project, args)

    print
    yml
    return


def combine_fastqs(path, num, project, sample, trueno):
    list_files = []
    for reads in os.listdir(path):
        path_split = path.split('/')
        reads_split = reads.split('_')
        if not os.path.isdir('/STORAGE/tmp/' + path_split[-2]):
            cmd = 'mkdir ' + '/STORAGE/tmp/' + path_split[-2]
            os.system(cmd)
        if num in reads_split:
            list_files.append(path + '/' + reads)
            new_path = '/STORAGE/tmp/' + path_split[-2] + '/' + '_'.join(
                reads_split[:-4]) + '_L432_S1_' + num + '_001.fastq.gz'

    string_files = ' '.join(list_files)
    cmd = 'cat {} > {}'.format(string_files, new_path)
    os.system(cmd)
    return new_path


def make_yml(fastq_list, ymlfile, project, args):
    with open(ymlfile, 'w') as f:
        f.write('#!/usr/bin/env cwl-runner\n')
        f.write('cwl:tool: resolve\n')
        if args.slack:
            f.write('# slack_username,' + args.slack + '\n')
        f.write('# analysis_start_time, -1\n')
        f.write('Reads:\n')

        for fastq in sorted(fastq_list):
            f.write(' - class: File\n')
            f.write('   location: {}\n'.format(fastq))
        f.write('\n')

        f.write('Label_Version: {}\n'.format(args.label_version))
        f.write('Barcode_Num: 65536\n')
        f.write('Internal_Run: True\n')
        f.write('Seq_Run: ' + project + '\n')

        f.write('Reference:\n')
        ref_path = '/STORAGE/Resolve/gene-panel/' + args.ref
        f.write(' - class: File\n')
        f.write('   location: {}\n'.format(ref_path))

        if args.abseq:
            f.write('Abseq Reference:\n')
            ref_path = '/STORAGE/Resolve/abseq-panel/' + args.abseq
            f.write(' - class: File\n')
            f.write('   location: {}\n'.format(ref_path))

        if args.subsample:
            f.write('Subsample: {}\n'.format(args.subsample))
            if args.seed:
                f.write('Subsample_seed: {}\n'.format(args.seed))

        if args.bam:
            f.write('Bam_Input:\n')
            for bam in args.bam.split(','):
                f.write(' - class: File\n')
                f.write('   location: {}\n'.format(bam))

        if args.trueno:
            f.write('Sample_Tags_Version: ' + args.trueno)

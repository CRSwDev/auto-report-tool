#Author: Imteaz Siddique
#Scope: This script compiles the datatables and then calculates the subsampling into one analysis.

import RhapTableGen
import Subsample
import YMLgen
import argparse

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--listdirect', nargs='+')
    parser.add_argument('--output', type=str, help='add name of output csv file')
    parser.add_argument('--RSEC', type=bool, default=False,
                        help="Type 'True' to subsample to RSEC. False to subsample to Raw reads. Default is False.")

    pathList = parser.parse_args().listdirect

    inprocessdf = RhapTableGen.RhapTable(pathList)
    subData = Subsample.SubSample(inprocessdf)
    YMLgen.ymlgen(subData, pathList)

if __name__ == '__main__':
    main()
